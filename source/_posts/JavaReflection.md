---
title: 说说java反射机制
date: 2020/4/22 17:01:30
tags:
  - redis
index_img: /img/index.jpg
banner_img: /img/index.jpg
categories:
  - Java
---

## 定义

Java 的反射（reflection）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。

## 能做什么

## 几种方式

1. 使用 class.forName(String classPath)
   > classPath：写需要反射的类名，一般是以包名.类名
   > 注意事项：这里会产生一个 ClassNotFoundException 异常，我们需要将异常处理或者抛出
   > 返回值：Class 对象
   ```
      try {
            Class clz = Class.forName("com.entity.Book");
      } catch (ClassNotFoundException e) {
            e.printStackTrace();
      }
   ```
2. 直接使用 Class clz = 类名.class,这种情况一般在我们知道有这个类的时候去使用
   ```
   Class clz = Book.class;
   ```
3. Class clz = 对象. getClass();前提是对象已经被实例化出来了
   ```
   Book book = new Book();
   Class clz = book.getClass();
   ```
