---
title: SpringBoot自动配置的实现原理
date: 2020/4/24 15:39:30
tags:
  - redis
index_img: /img/index.jpg
banner_img: /img/index.jpg
categories:
  - Java
---

## 测试  失敬失敬

1. springboot 启动的时候加载主配置类，通过@EnableAutoConfiguration 开启了自动配置功能
2. @EnableAutoConfiguration 利用 AutoAutoConfigurationImportSelector 给容器中导入一些组件。
