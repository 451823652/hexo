---
title: SpringMVC的简介和工作流程
date: 2020/4/5 10:55:48
tags:
  - Spring
index_img: /blogimg/spring.png
banner_img: /img/index.jpg
categories:
  - Java
---

Spring MVC 框架主要由 DispatcherServlet、处理器映射、控制器、视图解析器、视图组成，其工作原理如图 1 所示。
![](/blogimg/SpringMVC.png)

1.前端请求发送到 dispatcherservlet

2.dispatcherservlet 收到请求调用 handlermapping 处理器映射器，由此得知，该请求哪个 Controller 来处理（并未调用 Controller，只是得知）

3.dispatcherservlet 调用 HandlerAdapter 处理器适配器，并告诉处理器适配器应该要执行哪个 Controller

4.HandlerAdapter 处理器适配器去执行 Controller 并得到 ModelAndView(数据和视图)，并层层返回给 DispatcherServlet

5、DispatcherServlet 将 ModelAndView 交给 ViewReslover 视图解析器解析，然后返回真正的视图。

6、DispatcherServlet 将模型数据填充到视图中

7、DispatcherServlet 将结果响应给用户
