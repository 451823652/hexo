---
title: 缓存穿透 缓存击穿 缓存雪崩
date: 2020/4/9 17:01:30
tags:
  - redis
index_img: /img/index.jpg
banner_img: /img/index.jpg
categories:
  - Redis
---

### 缓存穿透

描述：是数据库中没有的数据，缓存中也没有，而且用户不断发起请求，比如主键不存的数据，这是用户很可能是攻击者，攻击会导致数据库压力过大

方案：

- 缓存空对象：代码简单，但效果不是很好，每次访问一个不存在的数据，都会去查一次数据库,或者 id<0 的直接拦截
- 布隆过滤器：代码维护复杂，效果很好

### 缓存击穿

描述：数据库中有的数据，但是缓存中没有，一般热点数据，并发访问

方案：

1. 设置的过期时间，数据刚好失效
2. 加互次锁，可以做到只查询一次数据库，再次读取上一个线程插入的缓存

```
 public static String getData(String key) throws InterruptedException{
        //从缓存中读取数据
        String result=getDataFromRedis(key);
        //缓存中不存在数据
        if(result==null){
            ReentrantLock reentrantLock = new ReentrantLock();
            if(reentrantLock.tryLock()){
                try {
                    //从数据库获取数据
                    result=getDataFromMysql(key);
                    if(result!=null){
                        setDataToCache(key,result);
                    }
                }finally {
                    reentrantLock.unlock();
                }
            }else {
                //暂停100ms再重新获取数据
                Thread.sleep(100);
                result=getDataFromRedis(key);
            }
        }
        return result;
    }
```

### 缓存雪崩

描述：数据库宕机，大部分数据失效

方案：

1.  缓存数据的过期时间设置随机，防止同一时间大量数据过期现象发生，错开数据失效时间
2.  如果缓存数据库是分布式部署，将热点数据均匀分布在不同缓存数据库中，即 Rediscluster->高可用集群
3.  设置热点数据永远不过期

## 使用过 Redis 分布式锁吗？怎么实现？

先拿 setnx 来争抢锁，抢到之后，再用 expire 给锁加一个过期时间防止锁忘记了释放
如果在 setnx 之后执行 expire 之前进程意外 crash 或者要重启维护了，那会这样？
set 指令有非常复杂的参数，这个应该是可以同时把 setnx 和 expire 合成一条指令来用
