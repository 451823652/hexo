---
title: 说说线程池的理解
date: 2020/4/5 11:22:56
tags:
  - threadpool
index_img: /img/index.jpg
banner_img: /img/index.jpg
categories:
  - Java
---

首先线程池位于 java.util.concurrent 包下，线程池创建的时候里面没有创建线程，默认在有任务提交的时候才会创建

## 线程池的意义

线程是稀缺资源，它的创建与销毁是一个相对偏重且耗资源的操作，而 java 线程依赖于内核线程(KLT),
创建线程需要进行<font face="黑体" color=red>操作系统状态的切换</font>，为避免资源过度消耗需要设法<font face="黑体" color=red>重用线程</font>执行多个任务。线程池
就是一个线程缓存，负责对线程进行统一分配，调优与监控。

## 什么时候使用线程池？

- 单个任务处理时间比较短
- 需要处理的任务数据很大

## 线程池优势

- 重用存在的线程，减少线程创建，消亡的开销，提高性能
- 提高响应速度，当任务到达时，任务可以不需要的等到线程创建就能立即执行
- 提高线程的可管理性，可统一分配，调优和监控

## 线程池分类有四种类型

- newFixedThreadPool()

  > 说明：初始化一个指定线程数的线程池，其中 corePoolSize == maxiPoolSize，使用 LinkedBlockin gQuene 作为阻塞队列 特点：即使当线程池没有可执行任务时，也不会释放线程。

- newCachedThreadPool()

  > 说明：初始化一个可以缓存线程的线程池，默认缓存 60s，线程池的线程数可达到 Integer.MAX_VA LUE，即 2147483647，内部使用 SynchronousQueue 作为阻塞队列； 特点：在没有任务执行时，当线程的空闲时间超过 keepAliveTime，会自动释放线程资源；当提交新 任务时，如果没有空闲线程，则创建新线程执行任务，会导致一定的系统开销； 因此，使用时要注意控制并发的任务数，防止因创建大量的线程导致而降低性能。

- newSingleThreadExecutor()

  > 说明：初始化只有一个线程的线程池，内部使用 LinkedBlockingQueue 作为阻塞队列。 特点：如果该线程异常结束，会重新创建一个新的线程继续执行任务，唯一的线程可以保证所提交任 务的顺序执行

- newScheduledThreadPool()
  > 特定：初始化的线程池可以在指定的时间内周期性的执行所提交的任务，在实际的业务场景中可以使 用该线程池定期的同步数据。

> 总结：除了 newScheduledThreadPool 的内部实现特殊一点之外，其它线程池内部都是基于 Thread PoolExecutor 类（Executor 的子类）实现的。

## 线程池的五种状态

1. Running
   能接受新任务以及处理已添加的任务
2. Shutdown
   不接受新任务，可以处理已经添加的任务
3. Stop
   不接受新任务，不处理已经添加的任务，并且中断正在处理的任务
4. Tidying
   所有的任务已经终止，CTL 记录的"任务数量"为 0，CTL 负责记录线程池的运行状态与活动线程数量
5. Terminated
   线程池彻底终止，则线程池转变为 terminated 状态

## 线程池工作原理

![](/blogimg/线程池工作原理.jpg)

1. 如果当前运行的线程少于 corePoolSize，则会创建新的线程来执行新的任务
2. 如果运行的线程个数等于或者大于 corePoolSize，则会将提交的任务存放到阻塞队列 BlockingQueue 中
3. 如果当前 BlockingQueue 队列已满的话，则会创建新的线程来执行任务
4. 如果线程个数已经超过了 maximumPoolSize，则会使用饱和策略 RejectedExecutionHandler 来进行处理

## 线程池底层实现类 ThreadPoolExecutor 类

![](/blogimg/threadpool.png)
1.corePoolSize 核心线程数。在创建了线程池后，线程中没有任何线程，等到有任务到来时才创建线程去执行任务。默认情况下，在创建了线程池后，线程池中的线程数为 0，当有任务来之后，就会创建一个线程去执行任务，当线程池中的线程数目达到 corePoolSize 后，就会把到达的任务放到缓存队列当中。

2.maximumPoolSize：最大线程数。表明线程中最多能够创建的线程数量。

3.keepAliveTime：空闲的线程保留的时间。

4.TimeUnit：空闲线程的保留时间单位。

5.BlockingQueue<Runnable>：阻塞队列，存储等待执行的任务。

> 用来保存等待被执行的任务的阻塞队列，且任务必须实现 Runable 接口，在 JDK 中提供了如下阻塞 队列：
> 1.ArrayBlockingQueue：基于数组结构的有界阻塞队列，按 FIFO 排序任务；
> 2.LinkedBlockingQuene：基于链表结构的阻塞队列，按 FIFO 排序任务，吞吐量通常要高于 Array BlockingQuene；
> 3.SynchronousQuene：一个不存储元素的阻塞队列，每个插入操作必须等到另一个线程调用移除 操作，否则插入操作一直处于阻塞状态，吞吐量通常要高于 LinkedBlockingQuene；
> 4.priorityBlockingQuene：具有优先级的无界阻塞队列；

6.ThreadFactory：线程工厂，用来创建线程,通过自定义的线程工厂可以给每个新建的线程设置一个具有识别度的线程名。

7.RejectedExecutionHandler：队列已满，而且任务量大于最大线程的异常处理策略

## 线程池的拒绝策略

> 发生条件 <font face="黑体" color=red>当提交的任务数大于（workQueue.size() + maximumPoolSize ），就会触发线程池的拒绝策略。</font>

JDK 自带的有以下四种

- AbortPolicy:丢弃任务并抛出 RejectedExecutionException 异常  (<font face="黑体" color=red>默认</font>)

- DiscardPolicy：也是丢弃任务，但是不抛出异常

- DiscardOldestPolicy：丢弃队列最前面的任务，执行后面的任务

- CallerRunsPolicy：由调用线程处理该任务

### AbortPolicy 源码：

```java
 /**
     * A handler for rejected tasks that throws a
     * {@code RejectedExecutionException}.
     */
    public static class AbortPolicy implements RejectedExecutionHandler {
        /**
         * Creates an {@code AbortPolicy}.
         */
        public AbortPolicy() { }

        /**
         * Always throws RejectedExecutionException.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         * @throws RejectedExecutionException always
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            throw new RejectedExecutionException("Task " + r.toString() +
                                                 " rejected from " +
                                                 e.toString());
        }
    }
```

从源码中可以看到直接抛出了一个 RejectedExecutionException 异常

### DiscardPolicy 源码：

```java
  public static class DiscardPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardPolicy}.
         */
        public DiscardPolicy() { }

        /**
         * Does nothing, which has the effect of discarding task r.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        }
    }
```

源码中 rejectedExecution 方法没有做任何处理，直接丢弃任务

### DiscardOldestPolicy 源码

```java
 public static class DiscardOldestPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardOldestPolicy} for the given executor.
         */
        public DiscardOldestPolicy() { }

        /**
         * Obtains and ignores the next task that the executor
         * would otherwise execute, if one is immediately available,
         * and then retries execution of task r, unless the executor
         * is shut down, in which case task r is instead discarded.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                e.getQueue().poll();
                e.execute(r);
            }
        }
    }
```

由源码可知如果线程池没有 shutdown，那么就讲队列中的任务 poll 出来一个(对于这个任务什么也不做，就是丢弃掉了最早的任务)；然后在调用 execute()方法执行刚提交的任务(扔到队列里面),实际处理的任务会少于提交的任务总数

### CallerRunsPolicy 源码

```java
 public static class CallerRunsPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code CallerRunsPolicy}.
         */
        public CallerRunsPolicy() { }

        /**
         * Executes task r in the caller's thread, unless the executor
         * has been shut down, in which case the task is discarded.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                r.run();
            }
        }
    }
```

由调用线程处理该任务

> 测试代码

```java
public class text {
    public static void main(String[] args) {
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(10);
        RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy ();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10,
                5, TimeUnit.SECONDS, workQueue, handler);
        for(int i=0; i<200; i++) {
            try {
                executor.execute(new Thread(() -> System.out.println(Thread.currentThread().getName() + " is running")));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        executor.shutdown();
    }
}
```

从结果中可以看出，由调用线程处理该任务
![](/blogimg/threadpool-test-result.png)

## 线程池的关闭

关闭线程池，可以通过<font face="黑体" color=red>shutdown</font> 和 <font face="黑体" color=red>shutdownNow</font> 这两个方法。它们的原理都是遍历线程池中所有的线程，然后依次中断线程 <font face="黑体" color=red>shutdown</font> 和 <font face="黑体" color=red>shutdownNow</font> 还是有不一样的地方：

- shutdownNow 首先将线程池的状态设置为 STOP,然后尝试停止所有的正在执行和未执行任务的线程，并返回等待执行任务的列表,会直接中断正在执行的任务

- shutdown 只是将线程池的状态设置为 SHUTDOWN 状态，然后中断所有没有正在执行任务的线程,会将正在执行的任务继续执行完

> 调用了这两个方法的任意一个，isShutdown 方法都会返回 true，当所有的线程都关闭成功，才表示线程池成功关闭，这时调用 isTerminated 方法才会返回 true。

## 总结

四种拒绝策略是相互独立无关的，选择何种策略去执行，还得结合具体的业务场景。实际工作中，一般直接使用 ExecutorService 的时候，都是使用的默认的 defaultHandler ，也即 AbortPolicy 策略。
