---
title: linux与windows中启动java -jar 后台运行程序
date: 2020/8/9 17:01:30
tags:
  - redis
index_img: /blogimg/start-jar/start-logo.jpg
banner_img: /img/index.jpg
categories:
  - Redis
---

## Linux
直接用java -jar xxx.jar，当退出或关闭shell时，程序就会停止掉。以下方法可让jar运行后一直在后台运行。
	1. ```java -jar xxx.jar &  ```
	2. 
	    a. 执行```java -jar xxx.jar```后
	    b. ctrl+z 退出到控制台,执行 bg
	    c. Exit
	完成以上3步，退出SHELL后，jar服务一直在后台运行。
    3.
	``` nohup java -jar xxxx.jar & ```
	将java -jar xxxx.jar 加入  nohup   &中间，也可以实现
### linux启动脚本
```
#!/bin/bash
#这里可替换为jar包名字
APP_NAME=xixien-0.0.1.jar
#根据实际情况修改参数
#JVM="-server -Xms2g -Xmx2g -Xmn512m -XX:PermSize=128M -XX:MaxNewSize=128m -XX:MaxPermSize=25zh6m -Xss256k  -Djava.awt.headless=true -XX:+CMSClassUnloadingEnabled -XX:+CMSPermGenSweepingEnabled -Xloggc:/logs/xxx-server/GC/xxx-gc.log"
#APPFILE_PATH="-Dspring.config.location=/usr/local/config/application.properties"
#使用说明,用来提示输入参数
usage() {
	echo "Usage: sh 执行脚本.sh [start|stop|restart|status|log|backup]"
	exit 1
}
#检查程序是否在运行
is_exist() {
	pid=$(ps -ef | grep $APP_NAME | grep -v grep | awk '{print $2}')
	#如果不存在返回1,存在返回0
	if [ -z "${pid}" ]; then
		return 1
	else
		return 0
	fi
}

#启动方法
start() {
	is_exist
	if [ $? -eq "0" ]; then
		echo "${APP_NAME} is already running. pid=${pid} ."
	else
		#nohup java $JVM -jar $APPFILE_PATH $APP_NAME > /dev/null 2>&1
		#后台启动jar包，且控制环境变量，根据实际情况修改吧。
		#nohup java $JVM -jar $APP_NAME --spring.profiles.active=prod > /dev/null 2>&1 &
		nohup java -jar $APP_NAME > /home/xixien.out 2>&1 &
		echo "${APP_NAME} start success"
	fi
}

#停止方法
stop() {
	is_exist
	if [ $? -eq "0" ]; then
		kill -9 $pid
		echo "kill ${APP_NAME}  success"
	else
		echo "${APP_NAME} is not running"
	fi
}

#输出运行状态
status() {
	is_exist
	if [ $? -eq "0" ]; then
		echo "${APP_NAME} is running. Pid is ${pid}"
	else
		echo "${APP_NAME} is NOT running."
	fi
}
#重启
restart() {
	stop
	start
}

#日志
log() {
	# 输出实时日志
	tail -n 100 -f /home/log/xixien.log
}

#备份
backup() {
	#根据需求自定义备份文件路径。
	BACKUP_PATH=/home/backup
	#获取当前时间作为备份文件名
	BACKUP_DATE=$(date +"%Y%m%d(%H:%M:%S)")
	echo 'backup file ->'$BACKUP_PATH$BACKUP_DATE'.jar'
	# 备份当前jar包
	# 使用指令 cp 将当前目录 test/ 下的所有文件复制到新目录 newtest 下
	# cp –r test/ newtest  
	#
	cp -r /home/$APP_NAME $BACKUP_PATH/$BACKUP_DATE'.jar'
	echo 'copy success'
}

#根据输入参数,选择执行对应方法,不输入则执行使用说明
case "$1" in
"start")
	start
	;;
"stop")
	stop
	;;
"status")
	status
	;;
"restart")
	restart
	;;
"log")
	log
	;;
"backup")
	backup
	;;
*)
	usage
	;;
esac

```
> 使用sh xxx.sh status
## Windows

1，启动服务  install.bat
```	
@echo off

rem 设置进程名称
set PROCESS_NAME=sentinel
rem 设置需要启动的jar路径 比如：C:\Users\ASUS\Desktop\note\sentinel\sentinel-dashboard-1.7.1.jar
set JAR_PATH=xxx.jar

set PORT=8080

rem 设置jdk路径，需要加上bin目录 
SET JAVA_HOME="C:\Program Files\jdk1.8.0_121\jdk1.8.0_121\bin"
rem 重命名javaw.exe为指定名
copy %JAVA_HOME%"\javaw.exe"  %JAVA_HOME%\%PROCESS_NAME%.exe
rem 使用指定名称的javaw.exe执行jar文件 并且制定端口号  
start "lock-server" %JAVA_HOME%\%PROCESS_NAME%.exe -jar %JAR_PATH% --server.port=%PORT%
echo success
pause
```
2，卸载服务  uninstall.bat
```
@echo off 
rem 设置进程名称
set PROCESS_NAME=sentinel
rem 设置jdk路径，需要加上bin目录 
SET JAVA_HOME=C:\Program Files\jdk1.8.0_121\jdk1.8.0_121\bin
taskkill -f -t -im %PROCESS_NAME%.exe
cd  %JAVA_HOME%
del %PROCESS_NAME%.exe
echo success
pause
```
3，停止端口 stop_by_port.bat
```
@echo off
rem 设置进程名称
set PROCESS_NAME=sentinel
rem 设置jdk路径，需要加上bin目录 
SET JAVA_HOME=C:\Program Files\jdk1.8.0_121\jdk1.8.0_121\bin
rem 设置启动的端口号
set port=8080
for f tokens=1-5 %%i in ('netstat -ano^findstr %port%') do taskkill pid %%m -t -f

cd  %JAVA_HOME%
del %PROCESS_NAME%.exe
echo success
pause
```
