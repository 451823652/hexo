---
title: spring cloud alibaba一
date: 2020/8/9 17:01:30
tags:
  - redis
index_img: /img/index.jpg
banner_img: /img/index.jpg
categories:
  - Redis
---

最近新公司有用到阿里的 nacos，自己在研究的过程中，但发现有许多暗坑，比如 springboot，springcould 以及 springcould alibaba 之间的版本对应关系等，于是记录一番

## 什么是 nacos

> Nacos 致力于帮助您发现、配置和管理微服务。Nacos 提供了一组简单易用的特性集，帮助您实现动态服务发现、服务配置管理、服务及流量管理。

> Nacos 帮助您更敏捷和容易地构建、交付和管理微服务平台。 Nacos 是构建以“服务”为中心的现代应用架构(例如微服务范式、云原生范式)的服务基础设施。

## [版本对应关系](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

![](/blogimg/nacos1/nacos-version.png)

我使用的是以下版本
SpringBoot：2.2.5.RELEASE
Spring Cloud Alibaba：2.2.1.RELEASE
Spring Cloud Version：Spring Cloud Hoxton.SR3

## pom 文件

### SpringBoot 版本

```
<parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.5.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
</parent>
```

### Spring Cloud Alibaba 与 Spring Cloud Version 版本

```
 <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Hoxton.SR3</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

### config以及discovery功能

```
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>com.alibaba.nacos</groupId>
                    <artifactId>nacos-client</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.alibaba.nacos/nacos-client -->
        <dependency>
            <groupId>com.alibaba.nacos</groupId>
            <artifactId>nacos-client</artifactId>
            <version>1.3.1</version>
        </dependency>
```
这个地方nacos-config默认依赖nacos-client.1.2.1有一个bug，会在控制台不停输出,后面我们会去看下源码就知道为什么了
![](/blogimg/nacos1/error.png)

## 首先在项目的resources目录下新建一个bootstrap-dev.yml，代码如下
此时nacos会加载两个配置一个是```common.properties```以及一个与spring.application.name开头+.properties的配置文件，我这加载的是```shop.properties```
```
spring:
  application:
    name: shop
  cloud:
    nacos:
      config: #配置中心
        server-addr: localhost:8848 #nacos地址
        file-extension: properties #文件格式 yaml
        namespace: 39b6da3c-1a30-43c5-9d71-fa4d6bd8639f #nacos命名空间
        timeout: 6000 # 客户端获取配置的超时时间
        extension-configs[0]: #通过此参数可以配置服务独有的参数,多个依次按extension-configs[1]配置
          data-id: common.properties #存放通用配置
        config-long-poll-timeout: 6000 #长轮询的超时时间
      discovery: #服务发现
        namespace: 39b6da3c-1a30-43c5-9d71-fa4d6bd8639f
        server-addr: localhost:8848
```
## 配置自动刷新bean
```
@RefreshScope 
public class A{

}
```