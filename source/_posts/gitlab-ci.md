---
title: 使用gitlab的CI/DI部署hexo前端博客
date: 2020/8/31 19:01:30
tags:
  - gitlab hexo
index_img: /blogimg/gitlab-ci/cd.jpg
banner_img: /img/index.jpg
categories:
  - 自动化部署
---
## 一、安装gitlab-runner
服务器：window server 2019
1. [下载安装包](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe)，下载完成后，将gitlab-runner-windows-xxx.exe名字改为gitlab-runner.exe
2. 在系统中新建一个存放的文件夹，这个文件夹将是gitlab-runner的工作目录以及exe文件的存放目录，比如C:\GitLab-Runner
3. 启动命令，打开powershell
```
 cd C:\GitLab-Runner
 .\gitlab-runner.exe install
 .\gitlab-runner.exe start
```
4. 停止服务
```
cd C:\GitLab-Runner
.\gitlab-runner.exe stop
```
5. 卸载服务
```
cd C:\GitLab-Runner
.\gitlab-runner.exe stop
.\gitlab-runner.exe uninstall
cd ..
rmdir /s GitLab-Runner
```
## 二、注册gitlab-runner
```
cd C:\GitLab-Runner
.\gitlab-runner.exe register
```
1. 回出现下面的信息，意思是输入自己的gitlab-ci服务器地址，我这用的是https://gitlab.com/，按enter进入下一步
![](/blogimg/gitlab-ci/step-1.png)
2. 输入token，这个token在gitlab中setting中，
![](/blogimg/gitlab-ci/step-2.png)
3. 按照下面步骤查看token，按enter进入下一步
![](/blogimg/gitlab-ci/step-3.png)
4. 输入一个描述信息，比如项目的名字
![](/blogimg/gitlab-ci/step-5.png)
5. 输入标记，比如：test，deploy
![](/blogimg/gitlab-ci/step-5.png)
6. 输入执行脚本的类型，我这选择shell
## 三、执行以上步骤后，在我们新建的C:\GitLab-Runner目录多个一个配置文件，内容如下

![](/blogimg/gitlab-ci/step-6.png)
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "hexo"
  url = "https://gitlab.com/"
  token = "你的token"
  executor = "shell"
  shell = "powershell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```
## 四、查看是否注册成功

![](/blogimg/gitlab-ci/step-7.png)
## 五、在项目跟根目录下添加.gitlab-ci.yml
gitlab-runner将根据此文件中的脚本内容去构建job

## 六、我的脚本（window）
```
stages:
  - build

build:
  stage: build
  tags:
    - build
  only:
    - master
  script:
    - cd C:\GitLab-Runner\builds\qUt3XRzP\0\451823652\hexo
    - npm install --registry=https://registry.npm.taobao.org
    - hexo clean
    - hexo generate
    - cd E:\software\nginx-1.19.2\nginx-1.19.2
    - .\nginx.exe -s quit
    - Remove-Item E:\software\nginx-1.19.2\nginx-1.19.2\html\shao\hexo\* -recurse
    - Copy-Item  "C:\GitLab-Runner\builds\qUt3XRzP\0\451823652\hexo\public\*" "E:\software\nginx-1.19.2\nginx-1.19.2\html\shao\hexo" -Recurse
    - cd E:\software\nginx-1.19.2\nginx-1.19.2
    - start nginx
    - echo "success"
```
## 七、Centos
### 1.查看系统位数命令
```
uname -a
```
输出
```
[root@VM-0-5-centos ~]# uname -a
Linux VM-0-5-centos 2.6.32-696.el6.x86_64 #1 SMP Tue Mar 21 19:29:05 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
```
### 2.对应命令,我的是Linux x86-64 ，选择第一条
> /usr/local/bin/为gitlab-runner下载完成后所在目录
```

# Linux x86-64
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Linux x86
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386

# Linux arm
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm

# Linux arm64
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm64

# Linux s390x
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binari     es/gitlab-runner-linux-s390x
```
### 3.添加执行权限
```
sudo chmod +x /usr/local/bin/gitlab-runner
```
### 4.添加用户
```
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```
### 5.安装并启动gitlab-runner进程
```
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
### 6.注册runner与Gitlab 绑定

```
sudo ./gitlab-runner register
```
